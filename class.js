window.onload = function () {
    var user = 'daev',
        urlJSON = "class.json",
        langColors = {
            "Ryan Z": "#c25975",
            "Raghav M": "#317aab",
            "Steven A": "#39add1",
            "Joshua L": "#637a91",
            "Joseph R": "#7d669e",
            "You": "#838cc7",
            "Databases": "#eb7728",
            "C#": "#9e4d83",
            "Game Development": "#228a8d",
            "Digital Literacy": "#c38cd4",
            "Java": "#2c9676",
            "Python": "#f095b2",
            "Business": "#f9845b",
            "Android": "#5cb860",
            "iOS": "#53bbb4",
            "Ruby": "#e15258",
            "Design": "#e59a13"
        },

        colorLine = {};

    //On a successful request, show the updated time
    $(document).ajaxSuccess(function (event, xhr, settings) {
        if (settings.url == urlJSON) {
            var dt = new Date();
            var time = dt.getHours() + ":" + dt.getMinutes() + ":" + dt.getSeconds();
            $('#status').text('Updated at ' + time);
        }
    });

    //On an unsuccessful request, show an error
    $(document).ajaxError(function (event, xhr, settings) {
        if (settings.url == urlJSON) {
            $('#status').text('Data not found!');
        }
    });

    $(document).ready(function () {

        //Make the request
        function refreshMetrics() {
            $('#status').text('Updating...');
            $.get(urlJSON, function (data, status) {
                data = JSON.parse(data);
                //To count the number of 'col-sm-3' elements in a row
                var rowCounter = 0;
                //Create the Profile area
                $('#gravatar').html("<img src='" + data['gravatar_url'] + "' />");
                $('#userName').html(data['name'] + " <small>(" + data['profile_name'] + ")</small>");

                //Create the Points area
                var totalPoints = 0;
                var pointsHTML = "<div class='row'>";

                //For each skill...
                $.each(data.points, function (skill, points) {
                    if (points > 0) {
                        //Leave out 'Total', put it in the profile area
                        if (skill == "total") {
                            totalPoints += points;
                            $('#totalPoints').html('Hours contributed: <span class="points">' + points + '</span>');
                        } else {
                            //Add a color to the color line array
                            colorLine[skill] = Math.round(100 * (points / totalPoints));
                            //Create a points box
                            pointsHTML += "<div class='col-sm-3 pointBox'>";
                            pointsHTML += "<div class='colorBar' style='background-color: " + langColors[skill] + "'></div>"
                            pointsHTML += skill + "<br /><span class='points'>" + points + "</span>";
                            pointsHTML += "</div>";
                            //Check how many 'col-sm-3' elements in a row (Bootstrap row)
                            rowCounter++;
                            if (rowCounter > 3) {
                                rowCounter = 0;
                                pointsHTML += "</div>";
                                pointsHTML += "<div class='row'>";
                            }
                        }
                    }
                });
                pointsHTML += "</div>";
                $('#pointsContainer').html(pointsHTML);

                //Create the Color Line
                var colorLineHTML = "";
                $.each(colorLine, function (skill, percent) {
                    colorLineHTML += "<div class='colorPiece' style='background:" + langColors[skill] + ";width:" + percent + "%;'></div>";
                });
                $("#colorLine").html(colorLineHTML);

                //Create the Badges area
                var badgesHTML = "";
                $.each(data.badges, function (i, badge) {
                    badgesHTML += "<p><img src='" + badge.icon_url + "' /> " + badge.name + "<br /><span class='small'>" + badge.earned_date.substring(0, 10) + "</span></p>";
                });
                $("#badgeTimeline").html(badgesHTML);
            });
        }

        //Do all the things
        refreshMetrics();

        //Do all the things every so often
        setInterval(function () { refreshMetrics(); }, 300000);

        //Do all the things when 'Update' is clicked
        $('#updateMetrics').on('click', function () {
            refreshMetrics();
        });

    });
};
