# Study Bank
This project was done for the Catalyst Hackathon, University of Melbourne 2018. Watch the demo here https://www.dropbox.com/s/vd7aq6uo0r12eyy/Catalyst2018Video.mov?dl=0&fbclid=IwAR2Dm-la0ivy8b05YdkmdIRTGotQI1EIWgq471SveUmtxBSP6RBThdBc3oE


## What?
Study Bank, is a platform designed to give students of all levels and ages the power to take control over their education. Study Bank is made up of the three features: 
1. Calendar: Have an overview of what your study habits are like with our calendar.
2. Study competition: Challenge your friends to a fun and a strengthening way to improve your education.
3. Ranking platform: Displaying all your of friends scores and ranking the number of hours they have studied!
