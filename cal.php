<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>title</title>
    <link rel="stylesheet" type="text/css" href="cass.css">
    <style>
        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            background-color: #1abc9c;
            overflow: hidden;
        }
        
        .menu li {
            float: left;
        }
        
        li a {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }
        
        li a:hover:not(.active) {
            background-color: #111;
        }
        
        .active {
            background-color:  #0a4e41;
        }
        </style>
  </head>
  
  
  
  <body>
      
  <?php include "Header.php";
createHeader("home");
?>

    <div class="month"> 
        <ul>
          <li>October<br><span style="font-size:18px">2018</span></li>
        </ul>
      </div>
      
      <ul class="weekdays">
        <li>Mo</li>
        <li>Tu</li>
        <li>We</li>
        <li>Th</li>
        <li>Fr</li>
        <li>Sa</li>
        <li>Su</li>
      </ul>
      
      <ul class="days"> 
        <a href="week1.php">
        <li>1</li>
        <li>2</li>
        <li>3</li>
        <li>4</li>
        <li>5</li>
        <li>6</li>
        <li>7</li>
        </a>
        <a href="week1.php">
        <li>8</li>
        <li>9</li>
        <li>10</li>
        <li>11</li>
        <li>12</li>
        <li>13</li>
        <li>14</li>
        </a>
        <a href="week1.php">
        <li>15</li>
        <li>16</li>
        <li>17</li>
        <li>18</li>
        <li>19</li>
        <li>20</li>
        <li>21</li>
        </a>
        <a href="week1.php">
        <li>22</li>
        <li>23</li>
        <li>24</li>
        <li>25</li>
        <li>26</li>
        <li>27</li>
        <li>28</li>
        </a>
        <a href="week5.html">
        <li>29</li>
        <li>30</li>
        <li>31</li>
        </a>
      </ul>
  </body>
</html>