
<?php 

function createHeader($currentPage){

?>
<style>
            ul {
                list-style-type: none;
                margin: 0;
                padding: 0;
                background-color: #1abc9c;
                overflow: hidden;
            }
            
            .menu li {
                float: left;
            }
            
            li a {
                display: block;
                color: white;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }
            
            li a:hover:not(.active) {
                background-color: #111;
            }
            
            .active {
                background-color:  #0a4e41;
            }
            </style>


<ul>
    <div class="menu">
        <li><a <?php if($currentPage == "calendar"){ 
            echo 'class="active"'; 
        }?>  href="cal.php">Calendar</a></li>
        <li><a <?php if($currentPage == "contest"){ 
            echo 'class="active"'; 
        }?>  href="competition.php">Contest</a></li>
        <li><a <?php if($currentPage == "profile"){ 
            echo 'class="active"'; 
        }?> href="rank.php">My Profile</a></li>
    </div>
</ul>
<?php

}
?>