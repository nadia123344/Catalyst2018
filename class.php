

<head>
        <meta charset="UTF-8">
        <title>title</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="class.css" />
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
                crossorigin="anonymous"></script>
        <script type="text/javascript" src="class.js"></script>
       
</head>


<body>
<?php include "Header.php";
createHeader("competition");
?>
        <div class="container-fluid">
                <div class="container py-2 ml-2">
                        <h1><img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/331813/treehouse.svg" />English<br /></h1>
                </div>
                <div class="container">
                        <div id="aboutUser" class="well py-2">
                                <div class="row">
                                        <div id="gravatar" class="col-sm-3">

                                        </div>
                                        <div class="col-sm-9">
                                                <p id="userName"></p>
                                                <p id="totalPoints"></p>
                                                <div id="colorLine"></div>
                                        </div>
                                </div>
                        </div>
                </div>
                <div class="container">
                        <div id="pointsContainer" class="well py-2 my-2">

                        </div>
                </div>
                <!-- <div class="container">
                        <div id="badgeTimeline" class="well my-2">

                        </div>
                </div> -->
        </div>
</body>


